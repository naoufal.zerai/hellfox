import React, { Component } from "react";
import { Modal, Input, Switch ,Select} from "antd";
import { Divider } from "./style";
import { CrudConsumer } from "./index";
const { Option } = Select;
export default class Create extends Component {
  _handleChange(e,r=null) {
    
    const { name, value } = e.target;
    if(r===null)
    this.setState({
      [name]: value
    });
    else
    this.setState({
      [name]: r
    });
  }
  _handleChangeOption  = name => val=>  {
    this.setState({
      [name]: val
    });
  };

  state = {};
  render() {
    return (
      <CrudConsumer>
        {e => (
          <Modal
            title="Create"
            okText="Send"
            onOk={() => e.insert(this.state)}
            onCancel={() => {
              this.props.close();
            }}
            visible={this.props.actif}
          >
            <form method="post" encType="multipart/form-data">
              {e.state.config.columns.map(val => {
                return (
                  <div key={val.key}>
                    {val.typeForm === "multiplechoice" && (
                      <Select
                        mode="multiple"
                        placeholder={"Select a person" + val.title}
                        optionFilterProp="children"
                        onChange={this._handleChangeOption(val.title)}
                        name={val.key}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {val.config.split(",").map(option => (
                          <Option value={option}>{option}</Option>
                        ))}
                      </Select>
                    )}
                    {val.typeForm === "choice" && (
                      <Select
                        showSearch
                        placeholder={"Select a person" + val.title}
                        optionFilterProp="children"
                        onChange={this._handleChangeOption(val.title)}
                        name={val.key}
                        filterOption={(input, option) =>
                          option.props.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                      >
                        {val.config.split(",").map(option => (
                          <Option value={option}>{option}</Option>
                        ))}
                      </Select>
                    )}
                    {val.typeForm === "date" && (
                      <Input
                        name={val.key}
                        placeholder={val.title}
                        value={this.state[val.key]}
                        onChange={e => this._handleChange(e)}
                      />
                    )}
                    {val.typeForm === "string" && (
                      <Input
                        name={val.key}
                        placeholder={val.title}
                        value={this.state[val.key]}
                        onChange={e => this._handleChange(e)}
                      />
                    )}
                    {val.typeForm === "bool" && (
                      <Switch
                        name={val.key}
                        onChange={(r, e) => this._handleChange(e, r)}
                      />
                    )}
                    {val.typeForm === "number" && (
                      <Input
                        name={val.key}
                        type="number"
                        placeholder={val.title}
                        value={this.state[val.key]}
                        onChange={e => this._handleChange(e)}
                      />
                    )}
                    {val.typeForm === "text" && (
                      <Input.TextArea
                        rows="4"
                        name={val.key}
                        placeholder={val.title}
                        value={this.state[val.key]}
                        onChange={e => this._handleChange(e)}
                      />
                    )}
                    <Divider />
                  </div>
                );
              })}
            </form>
          </Modal>
        )}
      </CrudConsumer>
    );
  }
}
