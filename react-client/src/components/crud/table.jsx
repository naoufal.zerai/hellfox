/* eslint-disable jsx-a11y/href-no-hash */
import React, { Component  } from "react";
import { CrudConsumer,Context } from "./index";
import { Table, Popconfirm, Icon } from "antd";
import { styles } from "./style";




export default class TableCrud extends Component {
  static contextType = Context;
  _actionsColumns() {
    return {
      title: "Actions",
      key: "actions",
      align:"right",
      width:50,
      render: item => (
        <CrudConsumer>
          {e => (
          <span key={e._id}>
            <a onClick={() => {e.updateValue('edit',true);e.updateValue('temp',item)}}>
              <Icon type="form" />
            </a> <a href="#" >
              <Popconfirm
                title="Remove this item?"
                onConfirm={() => e.delete(item._id)}
                icon={<Icon type="warning" style={styles.warningIcon} />}
              >
                <Icon type="delete" />
              </Popconfirm>
            </a>
          </span>
          )}
        </CrudConsumer>
      )
    };
  }

  
  render() {
    return (
      <CrudConsumer>
        {e => (
          <Table
            dataSource={e.state.data}
            columns={e.state.config.columns.concat(this._actionsColumns()).concat(this.props.actions)}
            rowKey="_id"
          />
        )}
      </CrudConsumer>
    );
  }
}
