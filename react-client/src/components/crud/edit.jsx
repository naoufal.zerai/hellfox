import React, { Component } from "react";
import { Modal, Input,Switch } from "antd";
import { Divider } from "./style";
import { CrudConsumer } from "./index";

export default class Edit extends Component {
  _handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  }
  state = {};
  componentWillReceiveProps(newProps){
    this.setState(newProps.object)
  }
  render() {
    return (
      <CrudConsumer>
        {e => (
          <Modal
            title="Edit"
            okText="Send"
            onOk={() => e.edit(this.state)}
            onCancel={() => {
              this.props.close();
            }}
            visible={this.props.actif}
          >
            <form method="post" encType="multipart/form-data">
              {e.state.config.columns.map(val => {
                return (
                  <div key={val.key}>
                    {val.typeForm === "string" && (
                      <Input
                        name={val.key}
                        placeholder={val.title}
                        value={this.state[val.key]}
                        onChange={e => this._handleChange(e)}
                      />
                    )}
                    {val.typeForm === "bool" && (
                      <Switch
                        name={val.key}
                        defaultChecked={this.state[val.key]}
                        onChange={(r, e) => this._handleChange(e, r)}
                      />
                    )}
                    {val.typeForm === "number" && (
                      <Input
                        name={val.key}
                        type="number"
                        placeholder={val.title}
                        value={this.state[val.key]}
                        onChange={e => this._handleChange(e)}
                      />
                    )}
                    {val.typeForm === "text" && (
                      <Input.TextArea
                        rows="4"
                        name={val.key}
                        placeholder={val.title}
                        value={this.state[val.key]}
                        onChange={e => this._handleChange(e)}
                      />
                    )}
                    <Divider />
                  </div>
                );
              })}
            </form>
          </Modal>
        )}
      </CrudConsumer>
    );
  }
}
