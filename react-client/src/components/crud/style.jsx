import styled from 'styled-components';
import { metrics } from '../../styles';

export const styles = {
    warningIcon: {
        color: 'red'
    },
    simpleIcon: {
        fontSize: '20px'
    },
    describeColumn: {
        wordWrap: 'break-word',
        maxWidth: 300,
        minHeight: 'auto'
    }
};
export const Divider = styled.div`
	margin: ${metrics.bigMargin} 0;
`;

export const FormIconWrapper = styled.a`
	font-size: 20px;
	margin: 0 ${metrics.mediumMargin};
	@media(max-width: 992px){
		margin: 0;
		display: block;
	}
`;
