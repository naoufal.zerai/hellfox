import React, { Component } from "react";
import TableCrud from "./table";
import Create from "./create";
import Edit from "./edit";
import { Icon, message, Button ,Switch} from "antd";
import Axios from "axios";
const CrudContext = React.createContext({});

export default class Crud extends Component {
  constructor(props) {
    super(props);



    this.state = {
      add: false,
      edit: false,
      temp: {},
      data: [],
      config: this.processConfig(this.props.config),
      id: this.props.id
    };
  }
  processConfig=(config)=>{
    
    config.columns.forEach(field => {
      if (field.typeForm==='bool') field.render = (e) => <Switch disabled={true} defaultChecked={e} />;
    }); 
    return config
  }
  componentDidMount() {
    this.getAllUsers();
  }

  updateValue = (key, val) => {
    this.setState({ [key]: val });
  };
  initProvider = config => {
    this.setState(config);
  };
  getAllUsers = () => {
    try {
      Axios.get(this.state.config.actions.list.endpoint).then(resp => {
        this.setState({ data: resp.data.data });
      });
    } catch (error) {
      message.error(error.message);
    }
  };
  insert = form => {
    try {
      Axios.post(this.state.config.actions.add.endpoint, form).then(res => {
        this.setState({ add: false });
        this.getAllUsers();
        message.success(JSON.stringify(form));
      });
    } catch (error) {
      message.error(error.message);
    }
  };
  edit = form => {
    try {
      Axios.put(this.state.config.actions.edit.endpoint, form).then(res => {
        this.setState({ edit: false });
        this.getAllUsers();
        message.success(JSON.stringify(form));
      });
    } catch (error) {
      message.error(error.message);
    }
  };
  delete = id => {
    try {
      Axios.delete(this.state.config.actions.delete.endpoint, {
        data: {
          _id: id
        }
      }).then(res => {
        this.setState({ add: false });
        this.getAllUsers();
        message.success(JSON.stringify(id));
      });
    } catch (error) {
      message.error(error.message);
    }
  };

  componentWillReceiveProps(nextProps, lastProps) {
    if (nextProps.id !== this.props.id) {
      this.setState({ config: nextProps.config, id: nextProps.id, data: [] });
    }
  }
  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.id !== this.state.id) {
      this.getAllUsers();
    }
  };

  render() {
    return (
      <CrudContext.Provider
        value={{
          state: this.state,
          updateValue: this.updateValue,
          insert: this.insert,
          edit: this.edit,
          delete: this.delete
        }}
      >
        <CrudContext.Consumer>
          {e => (
            <div>
              <Button
                type="primary"
                style={{ marginBottom: 16 }}
                onClick={() => {
                  e.updateValue("add", true);
                }}
              >
                <Icon type="plus" /> Add
              </Button>
              <TableCrud actions={this.props.actions}/>
              <Create
                actif={e.state.add}
                close={() => {
                  e.updateValue("add", false);
                }}
              />
              <Edit
                actif={e.state.edit}
                object={e.state.temp}
                close={() => {
                  e.updateValue("edit", false);
                }}
              />
            </div>
          )}
        </CrudContext.Consumer>
      </CrudContext.Provider>
    );
  }
}
export const CrudConsumer = CrudContext.Consumer;
export const CrudProvider = CrudContext.Provider;
export const Context = CrudContext;
