import React, { Component } from "react";
import { Layout, Menu, Breadcrumb, Icon, Input, Skeleton } from "antd";
import Crud from "../crud";
import Axios from "axios";
import { withRouter } from "react-router-dom";
const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout;
const { Search } = Input;


class Dashboard extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      loading:true,
      content: [],
      apps: [],
      general: [],
      shortcuts: [],
      breadcrumb: [],
      pageConfig: {},
      _id: null
    };
  }
  componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount() {
    this._isMounted = true;
    this.setState({ loading: true }, this.loadConfig);
  }
  loadConfig =  () => {
    Axios.get(process.env.REACT_APP_api_url + "config?name=sys")
      .then(resp => {
      
        if (this._isMounted)
          this.setState({     
            loading:true,       
            content: resp.data.data[0].content,
            apps: resp.data.data[0].apps,
            general: resp.data.data[0].general,
            shortcuts: resp.data.data[0].shortcuts,
            collapsed: false
          });

      })
      .then( async e => {
        if (typeof this.props.match.params.slug !== "undefined")
          await this.loadCrud(this.props.match.params.slug);
          let path = window.location.hash.split("/"); 
          this.setState({ loading: false, breadcrumb: [path[1], path[2]] });
      })
      .catch(e => console.log(e));
  }
  onCollapse = collapsed => {
    this.setState({ collapsed });
  };
  handleClick = e => {

    this.props.history.push("/"+e.keyPath[1]+"/" + e.keyPath[0]);

    this.setState({ breadcrumb: [e.keyPath[1], e.keyPath[0]] }, this.loadConfig)
    if (e.keyPath[1] === "content") {
      this.loadCrud(e.keyPath[0]);
    }  
  };
  capitalizeFirstLetter=(string="") => {
    if (string.length>0) return string.charAt(0).toUpperCase() + string.slice(1);
    else return ""
  };
  loadCrud = type => {
    return Axios.get(process.env.REACT_APP_api_url+"config?name=" + type)
      .then(resp => {
        if (this._isMounted)
          this.setState({
            pageConfig: {
              columns: resp.data.data[0].columns,
              actions: resp.data.data[0].actions
            },
            _id: resp.data.data[0]._id
          });
      })
      .catch(e => console.log(e));
  };

  render() {
    

    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Sider
          collapsible
          collapsed={this.state.collapsed}
          onCollapse={this.onCollapse}
        >
          {!this.state.collapsed && (
            <Search
              placeholder="Search"
              onSearch={value => console.log(value)}
              style={{ width: 160, margin: "16px" }}
            />
          )}

          <Menu
            mode="inline"
            defaultSelectedKeys={[
              window.location.hash.split("/").length > 2
                ? window.location.hash.split("/")[2]
                : ""
            ]}
            defaultOpenKeys={[
              window.location.hash.split("/").length > 1
                ? window.location.hash.split("/")[1]
                : ""
            ]}
            theme="dark"
            onClick={this.handleClick}
          >
            <SubMenu
              key="content"
              title={
                <span>
                  <Icon type="form" />
                  <span>Contents</span>
                </span>
              }
            >
              {this.state.content.map(e => (
                <Menu.Item key={e.caption}>{e.caption}</Menu.Item>
              ))}
            </SubMenu>
            <SubMenu
              key="apps"
              title={
                <span>
                  <Icon type="api" />
                  <span>Apps</span>
                </span>
              }
            >
              {this.state.apps.map(e => (
                <Menu.Item key={e.path}>{e.caption}</Menu.Item>
              ))}
            </SubMenu>
            <SubMenu
              key="shortcut"
              title={
                <span>
                  <Icon type="bulb" />
                  <span>Shortcuts</span>
                </span>
              }
            >
              {this.state.shortcuts.map(e => (
                <Menu.Item key={e.path}>{e.caption}</Menu.Item>
              ))}
            </SubMenu>
          </Menu>
        </Sider>

        <Layout>
          <Header style={{ background: "#fff", padding: 0 }}>
            <div className="logo" />
          </Header>
          <Content style={{ margin: "0 16px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}>
              {this.state.breadcrumb.map(e => (
                <Breadcrumb.Item key={e}>
                  {this.capitalizeFirstLetter(e)}
                </Breadcrumb.Item>
              ))}
            </Breadcrumb>

            <div style={{ padding: 24, background: "#fff", minHeight: 360 }}>
              {this.state.loading && <Skeleton active />}
              {!this.state.loading && 
              <div>
                {this.state.breadcrumb[0] === "content" && (
                  <Crud config={this.state.pageConfig} id={this.state._id} actions={{}}/>
                )}
                {this.state.breadcrumb[0] !== "content" && this.props.children}
              </div>
              }
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>
            ShellFox ©2019 Created by shellfox.com
          </Footer>
        </Layout>
      </Layout>
    );
  }
}
export default withRouter(Dashboard);
