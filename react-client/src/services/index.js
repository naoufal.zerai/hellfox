import axios from 'axios';
 
//Run with "php artisan serve"
export const api = axios.create({
         baseURL: process.env.REACT_APP_api_url,
         headers: {
           "Content-Type": "multipart/form-data"
         }
       });

//'Content-Type': 'multipart/form-data',