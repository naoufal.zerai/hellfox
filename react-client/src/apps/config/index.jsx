import React, { Component } from 'react'
import Dashboard from "../../components/Dashboard";
import {
    Form, 
    Tooltip,
    Input,
    Button,
    Upload,
    Icon,
} from 'antd';
class CONFIG_W extends Component {
    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    };

    normFile = e => {
        console.log('Upload event:', e);
        if (Array.isArray(e)) {
            return e;
        }
        return e && e.fileList;
    };

    render() {
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        return (
            <Dashboard>
            <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <Form.Item
                        label={
                            <span>
                                Site name&nbsp;
              <Tooltip title="What do you want others to call you?">
                                    <Icon type="question-circle-o" />
                                </Tooltip>
                            </span>
                        }
                    >
                        {getFieldDecorator('nickname', {
                            rules: [{ required: true, message: 'Please input your site name!', whitespace: true }],
                        })(<Input />)}
                    </Form.Item>


                <Form.Item label="Logo" extra="image">
                    {getFieldDecorator('upload', {
                        valuePropName: 'fileList',
                        getValueFromEvent: this.normFile,
                    })(
                        <Upload name="logo" action="/upload.do" listType="picture">
                            <Button>
                                <Icon type="upload" /> Click to upload
              </Button>
                        </Upload>,
                    )}
                </Form.Item>

                

                <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
                    <Button type="primary" htmlType="submit">
                        Submit
          </Button>
                </Form.Item>
            </Form>
            </Dashboard>
        );
    }
}

const CONFIG = Form.create({ name: 'validate_other' })(CONFIG_W);
export default CONFIG