import React, { Component } from 'react'
import Dashboard from "../../components/Dashboard";
import Crud from "../../components/crud";
import Axios from "axios";
import { Skeleton } from "antd";

export default class PAGE extends Component {
  _isMounted = false;
  constructor(props) {
    super(props)
    this.state = {
      loading:true,
      pageConfig: {},
      _id: null
    };
  }
componentWillUnmount() {
    this._isMounted = false;
  }
  componentDidMount() {
    this._isMounted = true;
    Axios.get(process.env.REACT_APP_api_url + "config?name=sys_page")
      .then(resp => {
        if (this._isMounted)
          this.setState({
            loading:false,
            pageConfig: {
              columns: resp.data.data[0].columns,
              actions: resp.data.data[0].actions
            },
            _id: resp.data.data[0]._id
          });
      })
      .catch(e => console.log(e));  
  }
  
  render() {
    return (
      <Dashboard nfl="test">
        {this.state.loading && <Skeleton active />}
        {!this.state.loading && (
          <Crud
            config={this.state.pageConfig}
            id={this.state._id}
            actions={{}}
          />
        )}
      </Dashboard>
    );
  }
}

