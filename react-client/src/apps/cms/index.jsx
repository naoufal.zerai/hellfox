import React, { Component } from 'react'
import Dashboard from "../../components/Dashboard";
import ListContent from "./listContent"

export default class CMS extends Component {
  render() {
    return (
      <Dashboard>
        <ListContent/>    
      </Dashboard>
    )
  }
}
