import React, { Component } from "react";
import { Layout, Menu, Button, Icon, Modal, Input } from "antd";
import Axios from "axios";
import FormContent from "./formContent";
const { SubMenu } = Menu;
const { Content, Sider } = Layout;

export default class List extends Component {
  componentDidMount() {
    this.load();
  }

  load = () => {
    Axios.get(process.env.REACT_APP_api_url +"config/content")
      .then(resp => {
        this.setState({
          contents: resp.data
        });
      })
      .catch(e => console.log(e));
  };

  state = {
    contents: [],
    selectedID: null,
    visible: false,
    newType: ""
  };
  showModal = () => {
    this.setState({
      visible: true
    });
  };

  createType = e => {
    Axios.post(process.env.REACT_APP_api_url +"config", {
      name: this.state.newType,
      published: false,
      type: "content",
      
        columns: [],
        actions: {
          add: {
            endpoint: process.env.REACT_APP_api_url +this.state.newType,
            group: ["admin"]
          },
          list: {
            endpoint: process.env.REACT_APP_api_url+this.state.newType,
            group: ["admin"]
          },
          delete: {
            endpoint: process.env.REACT_APP_api_url+this.state.newType,
            group: ["admin"]
          },
          edit: {
            endpoint: process.env.REACT_APP_api_url+this.state.newType,
            group: ["admin"]
          },
          view: {
            endpoint: process.env.REACT_APP_api_url+this.state.newType,
            group: ["admin"]
          }
        }      
    })
      .then(resp => {
        this.setState({
          visible: false
        });
        this.load();
      })
      .catch(e => console.log(e));
  };

  handleClick = e => {
    if (typeof e.keyPath !== "undefined") this.setState({ selectedID: e.key });
  };
  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  _handleChange(e) {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <Layout style={{ background: "#FFF" }}>
        <Modal
          title="New content type"
          visible={this.state.visible}
          onOk={this.createType}
          onCancel={this.handleCancel}
        >
          <Input
            name="newType"
            placeholder="Name"
            onChange={e => this._handleChange(e)}
          />
        </Modal>
        <Sider width={200}>
          <Menu
            mode="inline"
            defaultOpenKeys={["types"]}
            style={{ height: "100%" }}
            onClick={this.handleClick}
          >
            <SubMenu
              key="types"
              title={
                <span>
                  <Icon type="form" />
                  Content types
                </span>
              }
            >
              {this.state.contents.map(content => (
                <Menu.Item key={content._id}>{content.name}</Menu.Item>
              ))}
              <Menu.Item disabled={true}>
                <Button
                  type="link"
                  style={{ padding: 0 }}
                  onClick={this.showModal}
                >
                  <Icon type="plus" style={{ margin: 0 }} />
                  Add type
                </Button>
              </Menu.Item>
            </SubMenu>
          </Menu>
        </Sider>
        <Content style={{ padding: "0 24px", minHeight: 280 }}>
          <FormContent selectedID={this.state.selectedID} />
        </Content>
      </Layout>
    );
  }
}
