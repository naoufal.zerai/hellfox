import React, { Component } from "react";
import Axios from "axios";
import {
  Table,
  Button,
  Icon,
  Tag,
  Popconfirm,
  Modal,
  Input,
  Radio,
  Form,
  AutoComplete,
  Select,
  Row,
  Col,
  message,
  Switch as ASwitch
} from "antd";

const colorType = {
  string: "#a0d911",
  number: "#1890ff",
  bool: "#ad2102",
  date: "#ad8b00",
  file: "#00474f",
  text: "#780650",
  relation: "#722ed1",
  list: "#d46b08",
  double: "red",
  choice: "#254000",
  multiplechoice:"#9e1068"
};
const columns = [
  {
    title: "title",
    dataIndex: "title"
  },
  {
    title: "Type",
    dataIndex: "typeForm",
    render: type => (
      <Tag color={colorType[type]} key={type}>
        {type}
      </Tag>
    )
  },
  {
    title: "Actions",
    dataIndex: "actions",
    align: "right",
    render: item => (
      <span>
        <a onClick={() => {}}>
          <Icon type="form" />
        </a>{" "}
        <a href="#">
          <Popconfirm
            title="Remove this item?"
            onConfirm={() => {}}
            icon={<Icon type="warning" />}
          >
            <Icon type="delete" />
          </Popconfirm>
        </a>
      </span>
    )
  }
];
const radioStyle = {
  display: "block",
  height: "30px",
  lineHeight: "30px"
};
const dataSourceTables = ["1:1", "1:*", "*:*"];
const groups = ["admin", "g1", "Users", "Public"];
export default class FormContent extends Component {
  componentDidMount() {
    this.loadForm(this.props.selectedID);
  }
  componentWillReceiveProps(nextProps, lastProps) {
    if (nextProps.selectedID !== this.props.selectedID)
      this.loadForm(nextProps.selectedID);
  }

  loadForm = id => {
    try {
      if (id !== null)
        Axios.get(process.env.REACT_APP_api_url+"config/config?id=" + id).then(
          resp => {
            this.setState({
              config: {
                columns: resp.data.columns,
                actions: resp.data.actions
              },
              published: resp.data.published,
              id: id
            });
          }
        );
    } catch (error) {
      console.log(error);
    }
  };
  state = {
    config: {
      columns: []
    },
    selectedType: 1,
    visible: false,
    id: null,
    published: false,
    groupsRead: [],
    groupsWrite: [],
    mandatory:false
  };

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  createField = e => {
    let type, typeForm, config_;
    switch (this.state.selectedType) {
      case 1:
        type = "String";
        typeForm = "string";
        config_="";
        break;
      case 2:
        type = "Number";
        typeForm = "number";
        config_="";
        break;
      case 3:
        type = "Boolean";
        typeForm = "bool";
        config_="";
        break;
      case 7:
        type = "Date";
        typeForm = "date";
        config_="";
        break;
      case 4:
        //file
        type = "String";
        typeForm = "file";
        config_="";
        break;
      case 6:
        type = "String";
        typeForm = "text";
        config_="";
        break;
      case 8:
        type = "Array";
        typeForm = "list";
        config_="";
        break;
      case 9:
        type = "mongoose.Schema.Types.ObjectId";
        typeForm = "relation";
        config_="";
        break;
      case 5:
        type = "Number";
        typeForm = "double";
        config_="";
        break;
      case 10:
        type = "String";
        typeForm = "choice";
        config_=this.state.choice;
        break;
      case 11:
        type = "Array";
        typeForm = "multiplechoice";
        config_ = this.state.multiplechoice;
        break;

      default:
        break;
    }

    let columns = [
      ...this.state.config.columns,
      {
        title: this.state.newField,
        dataIndex: this.state.newField, //.toLowerCase(),
        type: type,
        typeForm: typeForm,
        config: config_,
        key: this.state.newField, //.toLowerCase(),
        read: this.state.groupsRead,
        write: this.state.groupsWrite,
        mandatory: this.state.mandatory
      }
    ];

    let config = {
      columns: columns,
      actions: this.state.actions
    };
    Axios.put(process.env.REACT_APP_api_url +"config", {
      _id: this.state.id,
      columns: config.columns
    })
      .then(resp => {
        this.setState({
          config: config,
          visible: false
        });
      })
      .catch(e => console.log(e));
  };
  handleCancel = e => {
    this.setState({
      visible: false
    });
  };
  publish = () => {
    Axios.get(
      process.env.REACT_APP_api_url +"config/publish?id=" + this.state.id
    );
  };
  _handleChangeRead = selectedItems => {
    this.setState({ groupsRead: selectedItems });
  };

  _handleChangeWrite = selectedItems => {
    this.setState({ groupsWrite: selectedItems });
  };
  _handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };
  onChangeRadio = e => {
    this.setState({
      selectedType: e.target.value
    });
  };
  removeType = () => {
    Axios.delete(process.env.REACT_APP_api_url +"config", {
      data: { _id: this.state.id }
    })
      .then(e => {
        message.success(JSON.stringify(e));
        
      })
      .catch(e => message.error(e.messgae));
  };
  _ismandatory=(e)=>{
    this.setState({
      mandatory:e
    })
  }
  render() {
    if (this.state.id !== null)
      return (
        <div>
          <Modal
            title="New field"
            visible={this.state.visible}
            onOk={this.createField}
            onCancel={this.handleCancel}
          >
            <Form>
              <Form.Item>
                <Input
                  name="newField"
                  placeholder="Name"
                  onChange={e => this._handleChange(e)}
                />
              </Form.Item>
              <Form.Item>
                Mandatory : <ASwitch onChange={r => this._ismandatory(r)} />
              </Form.Item>
              <Form.Item label="Type">
                <Radio.Group
                  onChange={this.onChangeRadio}
                  value={this.state.selectedType}
                  className="ant-row"
                >
                  <Col span={6}>
                    <Radio style={radioStyle} value={1}>
                      <Tag color="#a0d911">String</Tag>
                    </Radio>
                    <Radio style={radioStyle} value={2}>
                      <Tag color="#1890ff">Number</Tag>
                    </Radio>
                    <Radio style={radioStyle} value={3}>
                      <Tag color="#ad2102">Boolean</Tag>
                    </Radio>
                    <Radio style={radioStyle} value={8}>
                      <Tag color="#d46b08">List</Tag>
                    </Radio>
                  </Col>
                  <Col span={6}>
                    <Radio style={radioStyle} value={7}>
                      <Tag color="#ad8b00">Date</Tag>
                    </Radio>
                    <Radio style={radioStyle} value={4}>
                      <Tag color="#00474f">File</Tag>
                    </Radio>
                    <Radio style={radioStyle} value={5}>
                      <Tag color="red">Double</Tag>
                    </Radio>
                    <Radio style={radioStyle} value={6}>
                      <Tag color="#780650">Text</Tag>
                    </Radio>
                  </Col>

                  <Col span={6}>
                    <Radio style={radioStyle} value={11}>
                      <Tag color="#9e1068">Multiple-choice :</Tag>
                      {this.state.selectedType === 11 ? (
                        <Input name="multiplechoice" style={{ width: 100, marginLeft: 10 }} onChange={e => this._handleChange(e)}/>
                      ) : null}
                    </Radio>
                    <Radio style={radioStyle} value={10}>
                      <Tag color="#254000">Choice :</Tag>
                      {this.state.selectedType === 10 ? (
                        <Input name="choice" style={{ width: 100, marginLeft: 10 }} onChange={e => this._handleChange(e)}/>
                      ) : null}
                    </Radio>
                    <Radio style={radioStyle} value={9}>
                      <Tag color="#722ed1">Relation :</Tag>
                      {this.state.selectedType === 9 ? (
                        <AutoComplete
                          style={{ width: 100, marginLeft: 10 }}
                          dataSource={dataSourceTables}
                          placeholder="Selecct table "
                          filterOption={(inputValue, option) =>
                            option.props.children
                              .toUpperCase()
                              .indexOf(inputValue.toUpperCase()) !== -1
                          }
                        />
                      ) : null}
                    </Radio>
                  </Col>
                </Radio.Group>
              </Form.Item>

              <Row gutter={16}>
                <Col span={12}>
                  <Form.Item label="Read">
                    <Select
                      name="groupsRead"
                      mode="multiple"
                      placeholder="Groups"
                      value={this.state.groupsRead}
                      onChange={this._handleChangeRead}
                      style={{ width: "100%" }}
                    >
                      {groups.map(item => (
                        <Select.Option key={item} value={item}>
                          {item}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item label="Write">
                    <Select
                      name="groupsWrite"
                      mode="multiple"
                      placeholder="Groups"
                      value={this.state.groupsWrite}
                      onChange={this._handleChangeWrite}
                      style={{ width: "100%" }}
                    >
                      {groups.map(item => (
                        <Select.Option key={item} value={item}>
                          {item}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
            </Form>
          </Modal>
          <div style={{ marginBottom: 16 }}>
            {this.state.published === false && (
              <Button
                style={{ marginRight: 5 }}
                size="small"
                type="primary"
                onClick={this.publish}
              >
                Publish
              </Button>
            )}

            <Button size="small" type="danger" onClick={this.removeType}>
              Remove Type
            </Button>
          </div>
          <Table
            columns={columns}
            dataSource={this.state.config.columns}
            size="medium"
            showHeader={false}
            pagination={false}
          />
          <Button
            type="dashed"
            block
            style={{ marginTop: 16 }}
            onClick={this.showModal}
          >
            <Icon type="plus" /> Add field
          </Button>
        </div>
      );
    return [];
  }
}
