import React from 'react';
import { HashRouter, Switch, Route } from 'react-router-dom';
import {Result,Button} from 'antd'
import Home from '../apps/home/index';
import CMS from "../apps/cms/index";
import BATCH from "../apps/batch/index";
import PAGE from "../apps/page/index";
import FILE from "../apps/file/index";
import CONFIG from "../apps/config/index";

const E_404 = <Result
    status="404"
    title="404"
    subTitle="Sorry, the page you visited does not exist."
    extra={<Button type="primary">Back Home</Button>}
  />;

const Router = () => (
  <HashRouter>
    <Switch>
      <Route path="/" component={Home} exact />
      {/* CONTENT */}
      <Route path="/content/:slug" component={Home} exact />
      {/* APPS */}
      <Route path="/apps/cms" component={CMS} exact />
      <Route path="/apps/import" component={CMS} exact />
      <Route path="/apps/batch" component={BATCH} exact />
      <Route path="/apps/roles" component={CMS} exact />
      <Route path="/apps/files" component={FILE} exact />
      <Route path="/apps/pages" component={PAGE} exact />
      <Route path="/apps/config" component={CONFIG} exact />
      {/* 404 */}      
      <Route path="*" component={E_404} />
    </Switch>
  </HashRouter>
);

export default Router;