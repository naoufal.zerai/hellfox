const mongoose = require('mongoose');

const SYSCONFIG = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: { type: String, required: true },
  type: { type: String, required: true },
  published: Boolean,
  content:Array,
apps:Array,
general:Array,
shortcuts:Array,
actions:Object,
columns:Array
});

module.exports = mongoose.model("Config", SYSCONFIG);