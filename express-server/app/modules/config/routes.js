const Config = require.main.require("../core/models").Config;
const crud = require.main.require("../core/crud")(Config);
const mongoose = require("mongoose");
const Handlebars = require("handlebars");
const rimraf = require("rimraf");
var mkdirp = require("mkdirp-promise");
var fs = require("fs");

module.exports = (app, auth, module) => {
  app.get(module + "/", crud.readMany);
  app.post(module + "/", crud.create);
  app.delete(module + "/", (req, res) => {       
    Config.findOne({ name: "sys" }, (e, result) => {
      //get config in todel
      {
         var name,todel;
         Config.findById({ _id: req.body._id }, (e, ee) => {
           name = ee.name;
           Config.findOne(
             { _id: result._id },
             {
               content: {
                 $elemMatch: { name: name }
               }
             },
             (e, ee) => {
               todel = ee.content[0];
               Config.update(
                 { _id: result._id },
                 {
                   $pull: {
                     content: todel
                   }
                 },
                 e => {
                   Config.findOneAndDelete({ name: todel.name }, e => {
                     mongoose.connection.db.collection(
                       todel.name + "s",
                       (e, m) => {
                         m.drop();
                         let folder = __dirname + "/../" + todel.name;
                         rimraf(folder, () => {
                           res.send({ code: 100 });
                           process.exit(1);
                         });
                       }
                     );
                   });
                 }
               );
             }
           );
         });
        
    }

    }

    );
  });
  app.put(module + "/", crud.update);  
  app.get(module + "/content", (req, res) => {
    var query = Config.find({ type: "content" }).select("name _id");
    query.exec(function(err, result) {
      if (err) return next(err);
      res.send(result);
    });
  });
  app.get(module + "/config", (req, res) => {
    Config.findById({ _id: req.query.id }, (e, result) => {
      if (e) {
        res.status(500).send(e);
      } else {
        res.send(result);
      }
    });
  });
  app.get(module + "/init", (req, res) => {
    Config.collection.drop();
    //insert dashboard config
    Config.insertMany([
      {
        _id: new mongoose.Types.ObjectId(),
        content: [],
        apps: [],
        general: [],
        shortcuts: [],
        columns: [
          {
            title: "name",
            dataIndex: "name",
            type: "String",
            typeForm: "string",
            key: "name",
            read: [],
            write: [],
            mandatory: false
          }
        ],
        name: "sys_pages",
        published: false,
        type: "content",
        actions: {
          add: {
            endpoint: "http://localhost:3001/api/v1.0/sys_pages",
            group: ["admin"]
          },
          list: {
            endpoint: "http://localhost:3001/api/v1.0/sys_pages",
            group: ["admin"]
          },
          delete: {
            endpoint: "http://localhost:3001/api/v1.0/sys_pages",
            group: ["admin"]
          },
          edit: {
            endpoint: "http://localhost:3001/api/v1.0/sys_pages",
            group: ["admin"]
          },
          view: {
            endpoint: "http://localhost:3001/api/v1.0/sys_pages",
            group: ["admin"]
          }
        },
        __v: 0
      },

      {
        _id: new mongoose.Types.ObjectId(),
        content: [],
        apps: [],
        general: [],
        shortcuts: [],
        columns: [
          {
            title: "name",
            dataIndex: "name",
            type: "String",
            typeForm: "string",
            key: "name",
            read: [],
            write: [],
            mandatory: false
          },
          {
            title: "path",
            dataIndex: "path",
            type: "String",
            typeForm: "string",
            key: "path",
            read: [],
            write: [],
            mandatory: false
          },
          {
            title: "sti",
            dataIndex: "sti",
            type: "Array",
            typeForm: "list",
            key: "sti",
            read: [],
            write: [],
            mandatory: false
          }
        ],
        name: "sys_batch",
        published: false,
        type: "content",
        actions: {
          add: {
            endpoint: "http://localhost:3001/api/v1.0/sys_batch",
            group: ["admin"]
          },
          list: {
            endpoint: "http://localhost:3001/api/v1.0/sys_batch",
            group: ["admin"]
          },
          delete: {
            endpoint: "http://localhost:3001/api/v1.0/sys_batch",
            group: ["admin"]
          },
          edit: {
            endpoint: "http://localhost:3001/api/v1.0/sys_batch",
            group: ["admin"]
          },
          view: {
            endpoint: "http://localhost:3001/api/v1.0/sys_batch",
            group: ["admin"]
          }
        },
        __v: 0
      },

      {
        _id: new mongoose.Types.ObjectId(),
        content: [],
        apps: [],
        general: [],
        shortcuts: [],
        columns: [
          {
            title: "name",
            dataIndex: "name",
            type: "String",
            typeForm: "string",
            key: "name",
            read: [],
            write: [],
            mandatory: false
          },
          {
            title: "path",
            dataIndex: "path",
            type: "String",
            typeForm: "string",
            key: "path",
            read: [],
            write: [],
            mandatory: false
          }
        ],
        name: "sys_file",
        published: false,
        type: "content",
        actions: {
          add: {
            endpoint: "http://localhost:3001/api/v1.0/sys_file",
            group: ["admin"]
          },
          list: {
            endpoint: "http://localhost:3001/api/v1.0/sys_file",
            group: ["admin"]
          },
          delete: {
            endpoint: "http://localhost:3001/api/v1.0/sys_file",
            group: ["admin"]
          },
          edit: {
            endpoint: "http://localhost:3001/api/v1.0/sys_file",
            group: ["admin"]
          },
          view: {
            endpoint: "http://localhost:3001/api/v1.0/sys_file",
            group: ["admin"]
          }
        },
        __v: 0
      },

      {
        _id: new mongoose.Types.ObjectId(),
        content: [],
        apps: [],
        general: [],
        shortcuts: [],
        columns: [
          {
            title: "name",
            dataIndex: "name",
            type: "String",
            typeForm: "string",
            key: "name",
            read: [],
            write: [],
            mandatory: false
          },
          {
            title: "description",
            dataIndex: "description",
            type: "String",
            typeForm: "text",
            key: "description",
            read: [],
            write: [],
            mandatory: false
          }
        ],
        name: "sys_group",
        published: false,
        type: "content",
        actions: {
          add: {
            endpoint: "http://localhost:3001/api/v1.0/sys_group",
            group: ["admin"]
          },
          list: {
            endpoint: "http://localhost:3001/api/v1.0/sys_group",
            group: ["admin"]
          },
          delete: {
            endpoint: "http://localhost:3001/api/v1.0/sys_group",
            group: ["admin"]
          },
          edit: {
            endpoint: "http://localhost:3001/api/v1.0/sys_group",
            group: ["admin"]
          },
          view: {
            endpoint: "http://localhost:3001/api/v1.0/sys_group",
            group: ["admin"]
          }
        },
        __v: 0
      },

      {
        _id: new mongoose.Types.ObjectId(),
        content: [],
        apps: [],
        general: [],
        shortcuts: [],
        columns: [
          {
            title: "name",
            dataIndex: "name",
            type: "String",
            typeForm: "string",
            key: "name",
            read: [],
            write: [],
            mandatory: false
          },
          {
            title: "description",
            dataIndex: "description",
            type: "String",
            typeForm: "text",
            key: "description",
            read: [],
            write: [],
            mandatory: false
          }
        ],
        name: "sys_role",
        published: false,
        type: "content",
        actions: {
          add: {
            endpoint: "http://localhost:3001/api/v1.0/sys_role",
            group: ["admin"]
          },
          list: {
            endpoint: "http://localhost:3001/api/v1.0/sys_role",
            group: ["admin"]
          },
          delete: {
            endpoint: "http://localhost:3001/api/v1.0/sys_role",
            group: ["admin"]
          },
          edit: {
            endpoint: "http://localhost:3001/api/v1.0/sys_role",
            group: ["admin"]
          },
          view: {
            endpoint: "http://localhost:3001/api/v1.0/sys_role",
            group: ["admin"]
          }
        },
        __v: 0
      },

      {
        _id: new mongoose.Types.ObjectId(),
        content: [],
        apps: [],
        general: [],
        shortcuts: [],
        columns: [
          {
            title: "Name",
            dataIndex: "name",
            type: "String",
            typeForm: "string",
            key: "name",
            read: ["admin"],
            write: ["admin"]
          },
          {
            title: "Age",
            dataIndex: "age",
            type: "Number",
            typeForm: "number",
            key: "age",
            read: ["admin"],
            write: ["admin"]
          },
          {
            title: "Address",
            dataIndex: "adress",
            type: "String",
            typeForm: "text",
            key: "adress",
            read: ["admin"],
            write: ["admin"]
          }
        ],
        name: "hello",
        type: "content",
        published: true,
        actions: {
          add: {
            endpoint: "http://localhost:3000/api/v1.0/hello",
            group: ["admin"]
          },
          list: {
            endpoint: "http://localhost:3000/api/v1.0/hello",
            group: ["admin"]
          },
          delete: {
            endpoint: "http://localhost:3000/api/v1.0/hello",
            group: ["admin"]
          },
          edit: {
            endpoint: "http://localhost:3000/api/v1.0/hello",
            group: ["admin"]
          },
          view: {
            endpoint: "http://localhost:3000/api/v1.0/hello",
            group: ["admin"]
          }
        },
        __v: 0
      },

      {
        _id: new mongoose.Types.ObjectId(),
        content: [
          {
            caption: "Hello ",
            parrent: "",
            name: "hello"
          },
          {
            name: "sys_role",
            caption: "sys_role",
            parrent: ""
          },
          {
            name: "sys_group",
            caption: "sys_group",
            parrent: ""
          },
          {
            name: "sys_file",
            caption: "sys_file",
            parrent: ""
          },
          {
            name: "sys_batch",
            caption: "sys_batch",
            parrent: ""
          },
          {
            name: "sys_pages",
            caption: "sys_pages",
            parrent: ""
          }
        ],
        apps: [
          {
            caption: "Content type builder",
            path: "cms"
          },
          {
            caption: "Batch",
            path: "batch"
          },
          {
            caption: "File manager",
            path: "files"
          },
          {
            caption: "Page builder",
            path: "pages"
          },
          {
            caption: "Configuration",
            path: "config"
          }
        ],
        general: [],
        shortcuts: [],
        columns: [],
        name: "sys",
        type: "sys",
        __v: 0
      }]
    );
    
    res.sendStatus(200);
  });
  app.get(module + "/publish", (req, res) => {
    Config.findById({ _id: req.query.id }, (e, result) => {
      if (e) {
        res.status(500).send(e);
      } else {
        //if not exist
        let log = {};
        Config.countDocuments({
          "content.caption": result.name,
          name: "sys",
          type: "sys"
        }).then(nbr => {
          if (parseInt(nbr) === 0) {
            //add to sys
            Config.update(
              {
                name: "sys",
                type: "sys"
              },
              {
                $push: {
                  content: {
                    name: result.name,
                    caption: result.name,
                    parrent: ""
                  }
                }
              }
            ).then(uplog => (log.sys = uplog));
          }
        });

        //create backoffice module folder
        let folder = __dirname + "/../" + result.name;
        mkdirp(folder)
          .then(folder => mkdirp(folder + "/models"))
          .then(f => {
            var data = {
              name: result.name,
              cols: result.columns
            };

            fs.readFile(
              __dirname + "/template/route.hbs",
              "utf8",
              (err, contents) => {
                var templateRoutes = Handlebars.compile(contents);

                fs.readFile(
                  __dirname + "/template/model.hbs",
                  "utf8",
                  (err, contents) => {
                    var templateModel = Handlebars.compile(contents);
                    fs.writeFile(
                      folder + "/models/" + result.name + ".js",
                      templateModel(data),
                      routes => {
                        fs.writeFile(
                          folder + "/routes.js",
                          templateRoutes(data),
                          routes => {
                            process.exit(1);
                          }
                        );
                      }
                    );
                    
                  }
                );
              }
            );
          })
          .catch(err => console.error(err));
        res.send(log);
      }
    });
  });
};
