const passport = require('passport');
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = "secretz";
opts.passReqToCallback = true;

passport.use(new JWTstrategy(opts, (req,user,done) => {
    try {
      req.body.user = user;
      return done(null, user);
    } catch (error) {
      done(error);
    }
  })
);

module.exports = passport;
