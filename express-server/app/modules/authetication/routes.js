const User = require.main.require('../core/models').User;
var bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


module.exports = (app,auth,module) => {
    // ------ user ------------------
    app.get(module + '/user/auth', auth, (req, res) => { 
        console.log(req.body.user)
        res.status(200).send({ 'email': req.body.user.email, 'name': req.body.user.nom });
    });
    app.get(module + '/users', auth, (req, res) =>{
        return User.findAll()
            .then(Users => res.status(200).send(Users))
            .catch(error => res.status(400).send(error));
    });
    app.post(module + '/user', (req, res)=>{
        bcrypt.genSalt(10, function (err, salt) {
            bcrypt.hash(req.body.password, salt, function (err, hash) {
                console.log(hash)
                return User.create({
                    name: req.body.name,
                    email: req.body.email,
                    password: hash,
                    role: req.body.role || "user"
                })
                    .then(st => res.status(201).send(st))
                    .catch(error => res.status(409).send(error));
            });
        });
    });
    app.post(module + '/user/login', (req, res)=>{
        console.log(req.body)
        User.findOne({ where: { email: req.body.email } })
            .then(user => {
                if (user) {
                    bcrypt.compare(req.body.password, user.password)
                        .then((r) => {
                            if (r) {
                                //generer le token jwt
                                const token = jwt.sign({
                                    id: user.id,
                                    email: user.email,
                                    nom: user.name,
                                    role: user.role
                                }, "secret");
                                res.status(200).send({ token })
                            }
                            else res.status(403).send({ error: "Désolé, mot de passe incorrect !" })
                        })
                        .catch(error => { console.log(error); res.status(500).send(error) });
                }
                else res.status(403).send({ error: "Désolé, l'adresse email n'existe pas" })
            })
            .catch(error => { console.log(error); res.status(400).send(error) });
    });

    

}  