const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');
const models = {};

var mongoDB = 'mongodb://mongo:27017/myDB';
mongoose.connect(mongoDB);
// Get Mongoose to use the global promise library
mongoose.Promise = require('bluebird');
//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', () => {
  console.error.bind(console, 'MongoDB connection error:');
  console.log("Trying again to connect........");
  setTimeout(() => {
    mongoose.connect(mongoDB);
  }, 1000);

});

db.on('connected', function () {
  //isConnectedBefore = true;
  console.log('======>Connection established to MongoDB<=======');
});

//get all folders inside modules
let module_folder = __dirname+'/../app/modules'
fs
  .readdirSync(path.join(module_folder), { withFileTypes: true })
  .filter(folder => folder.isDirectory())
  .forEach(folder => {
    fs
      .readdirSync(path.join(module_folder, folder.name, 'models'))
      .filter(file => { return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js'); })
      .forEach(file => {
          
          var filepath = path.join(module_folder, folder.name, 'models', file);

          // When imported file use 'export default', object is assinged 'default'.
          var imported = (require(filepath).default) ?
            require(filepath).default :
            require(filepath);

          if (typeof imported.modelName !== 'undefined') {
            // Model definition file is expected exporting 'Model' of mongoose.
            models[imported.modelName] = imported;
          }
        });
  })
  models.db=db;

module.exports = models;