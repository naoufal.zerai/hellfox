const mongoose = require('mongoose');

module.exports = (Collection) => {

    
    // ======
    // Create
    // ======
    const create = (req, res) => {

        const newEntry = req.body;
        
        newEntry._id= new mongoose.Types.ObjectId(),

        Collection.create(newEntry, (e, newEntry) => {
            if (e) {
                console.log(e);
                res.sendStatus(500);
            } else {
                res.send(newEntry);
            }
        });
    };

    // =========
    // Read many
    // =========
    const readMany = (req, res) => {
        
        let query = req.query || {};

        Collection.find(query, (e, result) => {
            if (e) {
                res.status(500).send(e);
                console.log(e.message);
            } else {                
                res.send({data:result});
            }
        });
    };

    // ========
    // Read one
    // ========
    const readOne = (req, res) => {
        const {
            _id
        } = req.body._id;

        Collection.findById(_id, (e, result) => {
            if (e) {
                res.status(500).send(e);
                console.log(e.message);
            } else {
                res.send(result);
            }
        });
    };


    // ======
    // Update
    // ======
    const update = (req, res) => {
        const changedEntry = req.body;
        Collection.update(
          {
            _id: req.body._id
          },
          {
            $set: changedEntry
          },
          e => {
            if (e) res.sendStatus(500);
            else res.sendStatus(200);
          }
        );
    };

    // ======
    // Remove
    // ======
    const remove = (req, res) => {
        Collection.remove(
          {
            _id: req.body._id
          },
          e => {
            if (e) res.status(500).send(e);
            else res.sendStatus(200);
          }
        );
    };

    return {create,readMany,readOne,update,remove}



}