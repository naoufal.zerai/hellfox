'use strict';

const fs = require('fs');
const path = require('path');
const controllers = {};

//get all folders inside modules
let module_folder = __dirname + '/../app/modules'
fs
.readdirSync(path.join(module_folder), { withFileTypes: true })
.filter(folder => folder.isDirectory())
.forEach(folder => {
  //get all js files inside controllers folder in each module
  fs 
    .readdirSync(path.join(module_folder, folder.name,'controllers'))
    .filter(file => { return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');})
    .forEach(file => {
      controllers[file.substring(0, file.length-3)] = require(path.join(module_folder, folder.name,'controllers', file))
    });
})

module.exports = controllers;