'use strict';

const fs = require('fs');
const path = require('path');
const auth = require('../app/modules/authetication/middleware/auth').authenticate('jwt', { session: false });
const router = require('express').Router()

let module_folder = __dirname + '/../app/modules'

// get all folders inside modules
  fs
    .readdirSync(path.join(module_folder), { withFileTypes: true })
    .filter(folder => folder.isDirectory())
    // get all js files inside routes folder in each module
    .forEach(folder => {
        require(path.join(module_folder, folder.name,'routes'))(router,auth,'/'+folder.name);
    })

 var routes = [];
 router.stack.forEach(r => {
   routes.push([
     r.route.stack.filter(l => l.name != 'authenticate')[0].method,
     r.route.stack.filter(l => l.name != 'authenticate')[0].name,
     'http://localhost:3030/api/v1.0'+r.route.path
   ])
 })
router.get("/routes", (req, res) => {
  res.status(200).send(routes);
});

router.get("/restart", (req, res) => {
    process.exit(1);
});

module.exports = router
