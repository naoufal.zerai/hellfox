var createError = require('http-errors');
var express = require('express');
var logger = require('morgan');
const multer = require("multer");
const cors = require('cors');
const bodyParser = require('body-parser');
const listEndpoints = require('express-list-endpoints')
const router = require('./core/router');


const upload = multer();
var app = express();

app.use(logger('dev'));
// for parsing application/json
app.use(bodyParser.json()); 
// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true })); 
//form-urlencoded

// for parsing multipart/form-data
app.use(upload.array()); 

app.use(cors());

// Require our routes into the application.
app.use('/api/v1.0', router)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  //res.send('error');
  res.send(err.message);
});
console.log(listEndpoints(app));

module.exports = app;
